# Intro

To create aws session for local setup use

## Install

`go install bitbucket.org/rajkumargosavitp/aws-session-create@latest`

## Usage

```
aws-session-create --help                                                       
Usage of aws-session-create:
  -account_id string
        AWS Account ID
  -auth_code string
        Auth Code from the MFA app
  -username string
        AWS Account User name
```

`aws-session-create -auth_code=<6-digit code> -account_id=<accountID> -username=<username>`


You will get all the export statements

just copy the output and paste it in the terminal instance that you start your application from