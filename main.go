package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os/exec"
	"time"
)

type sessionRes struct {
	Credentials struct {
		AccessKeyId     string    `json:"AccessKeyId"`
		SecretAccessKey string    `json:"SecretAccessKey"`
		SessionToken    string    `json:"SessionToken"`
		Expiration      time.Time `json:"Expiration"`
	} `json:"Credentials"`
}

func main() {
	authCode := flag.String("auth_code", "", "Auth Code from the MFA app")
	accountID := flag.String("account_id", "", "AWS Account ID")
	username := flag.String("username", "", "AWS Account User name")
	flag.Parse()

	if len(*authCode) != 6 {
		log.Fatal("Invalid authCode should be length 6")
	}

	if *accountID == "" {
		log.Fatal("empty accountID")
	}

	if *username == "" {
		log.Fatal("empty username")
	}

	cmd, err := exec.Command("aws", "sts", "get-session-token", "--serial-number", fmt.Sprintf("arn:aws:iam::%s:mfa/%s", *accountID, *username), "--token-code", *authCode).Output()
	if err != nil {
		fmt.Println("@@@@", err)
		return
	}
	var creds sessionRes
	err = json.Unmarshal(cmd, &creds)
	if err != nil {
		fmt.Println("errr", err)
		return
	}
	fmt.Printf(`export AWS_DEFAULT_REGION=ap-southeast-1
	export AWS_SESSION_TOKEN=%v
	export AWS_SECRET_ACCESS_KEY=%v
	export AWS_ACCESS_KEY_ID=%v
	`, creds.Credentials.SessionToken, creds.Credentials.SecretAccessKey, creds.Credentials.AccessKeyId)
}
